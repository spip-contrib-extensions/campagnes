<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_campagne' => 'Ajouter cette publicitée',
	
	// C
	'champ_contextes_explication' => 'Indiquez un contexte par ligne pour lequel la publicité pourra être affichée, sous la forme <em>param=valeur</em>. Par exemple : <em>id_article=123</em> ou bien <em>page=sommaire</em>. Laissez vide si la publicité peut s’afficher sur toutes les pages.',
	'champ_contextes_label' => 'Liste des contextes où la publicité sera affichée',
	'champ_date_debut_label' => 'Date de début',
	'champ_date_fin_label' => 'Date de fin',
	'champ_descriptif_label' => 'Descriptif',
	'champ_id_annonceur_label' => 'Annonceur',
	'champ_id_encart_label' => 'Dans quel encart ?',
	'champ_lang_explication' => 'Si une langue est fournie dans le contexte de l’encart, il est possible de n’afficher la campagne que pour une langue donnée.',
	'champ_lang_label' => 'Restreindre à la langue',
	'champ_media_label' => 'Média',
	'champ_restrictions_affichage_label' => 'Restrictions d’affichage',
	'champ_restrictions_publication_explication' => 'Permettre de publier ou dépublier cette publicité suivant certaines options.',
	'champ_restrictions_publication_label' => 'Restrictions de publication',
	'champ_titre_label' => 'Titre',
    'champ_titre_explication'	=> 'Si le titre est vide, il essayera de se remplir magiquement.',
	'champ_url_label' => 'URL',
    'champ_url_explication'	=> 'L’URL d’une page (http://exemple) ou le raccourci d’un contenu SPIP (article123, rubrique456, etc).',

	
	// E
	'erreur_date' => 'Cette date n’existe pas.',
	'erreur_date_avant_apres' => 'La date de fin doit être égale ou après la date de début.',
	'erreur_date_deux' => 'Vous devez indiquez les deux dates, ou aucune.',
	'erreur_date_corrigee' => 'La date a été corrigée.',
	'erreur_upload_format' => 'Le format de ce fichier n\'est pas accepté.',
	'erreur_upload_publicite' => 'Impossible de télécharger votre publicité.',
	'erreur_upload_taille' => 'Les dimensions ne sont pas correctes. La taille de l\'encart est de @largeur@ &times; @hauteur@.',
	
	// I
	'icone_creer_campagne' => 'Créer une publicité',
	'icone_modifier_campagne' => 'Modifier cette publicité',
	'info_1_campagne' => 'Une publicité',
	'info_1_campagne_publie' => 'Une publicité publiée',
	'info_1_campagne_non_publie' => 'Une publicité non publiée',
	'info_aucun_campagne' => 'Aucune publicité',
	'info_aucun_campagne_publie' => 'Aucune publicité publiée',
	'info_aucun_campagne_non_publie' => 'Aucune publicité non publiée',
	'info_nb_campagnes' => '@nb@ publicités',
	'info_nb_campagnes_publies' => '@nb@ publicités publiées',
	'info_nb_campagnes_non_publies' => '@nb@ publicités non publiées',
	'info_campagnes_auteur' => 'Les publicités de cet auteur',
	
	// P
	'purger_statistiques_bouton' => 'Purger',
	'purger_statistiques_message_ok' => 'Toutes les statistiques ont été effacées !',
	'purger_statistiques_message_ok_date' => 'Statistiques effacées @debut_fin@.',
	'purger_statistiques_titre' => 'Purger les statistiques',
	
	// R
	'retirer_lien_campagne' => 'Retirer cette publicité',
	'retirer_tous_liens_campagnes' => 'Retirer toutes les publicités',
	
	// S
	'statistiques_campagne' => 'Statistiques de cette publicité',
	'statistiques_campagne_evolution' => 'Évolution de cette publicité',
	'statistiques_champ_clics' => 'Clics',
	'statistiques_champ_filtrer' => 'Filtrer les statistiques',
	'statistiques_champ_ratio' => 'Ratio (%)',
	'statistiques_champ_vues' => 'Vues',
	'statistiques_telecharger' => 'Télécharger l’évolution',
	'statut_obsolete_texte' => 'obsolète',
	'statut_prepa_texte' => 'inactive',
	
	// T
	'texte_ajouter_campagne' => 'Ajouter une publicité',
	'texte_changer_statut_campagne' => 'Cette publicité est :',
	'texte_creer_associer_campagne' => 'Créer et associer une publicité',
	'titre_langue_campagne' => 'Langue de cette publicité',
	'titre_logo_campagne' => 'Logo de cette publicité',
	'titre_campagne' => 'Publicité',
	'titre_campagnes' => 'Publicités',
	'titre_campagnes_rubrique' => 'Publicités de la rubrique',
);
